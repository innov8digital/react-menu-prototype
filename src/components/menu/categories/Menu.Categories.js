let Menu = Menu || {};

/**
 * Categories List
 * @description Top level categories
 * @type {jsx}
 */
Menu.Categories = class extends React.Component{
    constructor(){
        super();

        this.clickEventHandler = this.clickEventHandler.bind(this);

        this.state = {
            categories : this.initializeData(),
        };
    }

    initializeData(){
        return App.DataStore.getCategories() || [];
    }

    clickEventHandler(categoryId){
       Utilities.Common.Routes.redirect('/menu/' + categoryId + '/category');
    }

    assembleCategories(){
        const jsxCollection = [];
        for(let category of this.state.categories){
            jsxCollection.push(
                <Menu.CategoriesItem key={category.Id} categoryId={category.Id} categoryName={category.Name} categoryArtwork={category.Artwork} onClick={()=>this.clickEventHandler(category.Id)}/>
            );
        }

        return jsxCollection;
    };

    render(){
        const jsx = this.assembleCategories();
        return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12 bread-crumb">
                    <Link to={App.Config.setBasePath('/')} activeClassName="active">Menu</Link> <i className="fa fa-chevron-circle-right fa-lg text-shadow" aria-hidden="true"></i> <span className="current-view">Categories</span>
                </div>
            </div>
            <div className="row">
                {jsx}
            </div>
        </div>
        );
    }
};