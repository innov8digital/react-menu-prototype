let Menu = Menu || {};

/**
 * Categories List Item/Product
 * @type {jsx}
 */
Menu.CategoriesItem = class extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="col-md-4" onClick={() => this.props.onClick()}>
                <div className="categories-item center-block load-fadein">
                    <img src={App.Config.imagePath + this.props.categoryArtwork.background} className="img-responsive center-block" alt={"background image for " + this.props.categoryName}/>
                    <div className="categories-product-image-block">
                        <div className="categories-name">{this.props.categoryName}</div>
                        <img src={App.Config.imagePath + this.props.categoryArtwork.product} className="img-responsive img-rounded" alt={"product image for " + this.props.categoryName} title={"View all " + this.props.categoryName}/>
                    </div>
                </div>
            </div>
        );
    }
};

Menu.CategoriesItem.propTypes = {
    categoryId: React.PropTypes.number.isRequired,
    categoryName: React.PropTypes.string.isRequired,
    categoryArtwork: React.PropTypes.object.isRequired,
    onClick: React.PropTypes.func.isRequired
};