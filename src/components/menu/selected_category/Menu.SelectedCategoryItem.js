let Menu = Menu || {};

/**
 * Selected Category Item/Product
 * @type {jsx}
 */
Menu.SelectedCategoryItem = class extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="col-md-4" onClick={() => this.props.onClick()}>
                <div className="category-item center-block load-fadein">
                    <img src={App.Config.imagePath + this.props.itemArtwork.background} className="img-responsive center-block" alt={"background image for " + this.props.itemName}/>
                    <div className="category-product-image-block">
                        <div className="category-name">{this.props.itemName}</div>
                        <img src={App.Config.imagePath + this.props.itemArtwork.product} className="img-responsive img-rounded" alt={"product image for " + this.props.itemName} title={"View details for " + this.props.itemName}/>
                    </div>
                </div>
            </div>
        );
    }
};

Menu.SelectedCategoryItem.propTypes = {
    itemId: React.PropTypes.number.isRequired,
    itemName: React.PropTypes.string.isRequired,
    itemArtwork: React.PropTypes.object.isRequired,
    onClick: React.PropTypes.func.isRequired
};
