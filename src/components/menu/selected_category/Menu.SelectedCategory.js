let Menu = Menu || {};

/**
 * Selected Category List
 * @description Filtered category based on categories list selection
 * @type {jsx}
 */
Menu.SelectedCategory = class extends React.Component{
    constructor(props){
        super(props);

        this.initializeData = this.initializeData.bind(this);
        this.assembleCategoryItems = this.assembleCategoryItems.bind(this);
        this.clickEventHandler = this.clickEventHandler.bind(this);

        this.state = {
            categoryItems : this.initializeData()
        };
    }

    initializeData(){
        return App.DataStore.getCategoryItems(this.props.params.categoryId) || '';
    }

    clickEventHandler(productItemId){
        Utilities.Common.Routes.redirectWithState('/menu/' + productItemId + '/featured', {category : this.state.categoryItems});
    }

    assembleCategoryItems(){
        const jsxCollection = [];
        for(let item of this.state.categoryItems.ProductList){
            jsxCollection.push(
                <Menu.SelectedCategoryItem key={item.Id} itemId={item.Id} itemName={item.Name} itemArtwork={item.Artwork} onClick={()=>this.clickEventHandler(item.Id)}/>
            );
        }

        return jsxCollection;
    };

    render(){
        const jsx = this.assembleCategoryItems();
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12 bread-crumb">
                        <Link to={App.Config.setBasePath('/')} activeClassName="active">Menu</Link> <i className="fa fa-chevron-circle-right fa-lg text-shadow" aria-hidden="true"></i> <span className="current-view">{this.state.categoryItems.Name}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="category-list-featured-name">
                        Are Your Ready? Try These {this.state.categoryItems.Name}
                    </div>
                </div>
                <div className="row">
                    {jsx}
                </div>
            </div>
        );
    }
};

Menu.SelectedCategory.propTypes = {
    params: React.PropTypes.object.isRequired,
};