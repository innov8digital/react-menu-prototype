let Menu = Menu || {};

/**
 * Menu Item Detail
 * @description Featured  Menu Item Details
 * @type {jsx}
 */
Menu.Item = class extends React.Component{
    constructor(props){
        super(props);

        this.initializeData = this.initializeData.bind(this);

        this.state = {
            category : props.location.state.category,
            item : this.initializeData()
        };
    }

    initializeData(){
        return  App.DataStore.getItem(this.props.params.productId);
    }

    render(){
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12 bread-crumb">
                        <div className="bread-crumb">
                            <Link to={App.Config.setBasePath('/')} activeClassName="active">Menu</Link>  <i className="fa fa-chevron-circle-right fa-lg text-shadow" aria-hidden="true"></i>  <Link to={App.Config.setBasePath('/menu/' + this.state.category.Id + '/category')} activeClassName="active">{this.state.category.Name}</Link> <i className="fa fa-chevron-circle-right fa-lg text-shadow" aria-hidden="true"></i> <span className="current-view">{this.state.item.Name}</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="product-item center-block load-fadein">
                            <img src={App.Config.imagePath + this.state.item.Artwork.background_featured} className="img-responsive" alt={"background image for " + this.state.item.Name}/>
                            <div className="category-product-image-block clear">
                                <div className="product-name">{this.state.item.Name}</div>
                                <img src={App.Config.imagePath + this.state.item.Artwork.product} className="img-responsive img-rounded" alt={"product image for " + this.state.item.Name} title={"View details for " + this.state.item.Name}/>
                                <div className="description">
                                    <div className="tag-line">{this.state.item.Tagline}</div>
                                    {this.state.item.Description}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

Menu.Item.propTypes = {
    location: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired,
};