let Layout = Layout || {};

Layout.Footer = function(){
    return (
        <footer  id="footer" className="text-glow">
            <span className="hidden-sm hidden-xs">ComResource &bull; p.614-221-6348 &bull; <a href="http://comresource.com/" target="_blank">comresource.com</a>&nbsp;</span>
            <i className="fa fa-gear fa-fw text-glow-icn"></i> <span>Candidate : Bret Phillips &bull; <a href="http://innov8digital.com" target="_blank">innov8digital.com</a>
            <span className="hidden-sm hidden-xs">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://bitbucket.org/innov8digital/react-menu-prototype/downloads/" target="_blank"><i className="fa fa-download text-glow-icn"></i> Download Project Files</a></span>
           </span>
        </footer>
    );
};

