let Layout = Layout || {};

Layout.Header = class extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <header id="header" className="slideDown">
                <div className="row">
                    <div className="col-md-12">
                        <a href="https://www.wendys.com/" target="_blank" className="logo-img">
                            <img src={App.Config.imagePath + 'QIOR-en_US.png'} className="img-fluid hidden-sm hidden-xs" alt="Responsive image"/>
                        </a>
                        <div className="header-tagline">Hallo-Treats Menu</div>
                    </div>
                </div>
                {this.props.children}
            </header>
        );git
    }
};

Layout.Header.propTypes = {
    children: React.PropTypes.node.isRequired,
};