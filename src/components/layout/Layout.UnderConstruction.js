let Layout = Layout || {};

Layout.UnderConstruction = function(){
    return (
        <div className="row">
            <div className="cols-md-12">
                <div id="under-construction" className="center load-fadein">
                    <div className="error-code text-shadow">Under Construction</div>
                    <div className="error-message">
                        Opps! Regrettably.. we are undergoing construction... some assembly required...
                        <img src={App.Config.imagePath + 'bg-under-construction.png'} className="img-responsive center-block" alt="under construction"/>
                        <a href="javascript:void(0)" onClick={()=>{history.go(-1);}} className="btn btn-danger text-center"><i className="fa fa-arrow-left" aria-hidden="true"></i> Return to Prototype</a>
                    </div>
                </div>
            </div>
        </div>
    );
};