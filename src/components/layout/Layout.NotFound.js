let Layout = Layout || {};

Layout.NotFound = function(){
    return (
        <div className="row">
            <div className="cols-md-12">
                <div className="center load-fadein">
                    <div className="error-code text-shadow">Page Not Found (404)</div>
                    <div className="error-message">
                        Opps! Regrettably.. the page you are requesting cannot be located... this guy knows how you feel...
                        <img src={App.Config.imagePath + 'bg-404.png'} className="img-responsive center-block" alt="Route not found"/>
                        <a href="javascript:void(0)" onClick={()=>{history.go(-1);}} className="btn btn-danger text-center"><i className="fa fa-arrow-left" aria-hidden="true"></i> Return to Prototype</a>
                    </div>
                </div>
            </div>
        </div>
    );
};