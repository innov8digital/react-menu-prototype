let Utilities = Utilities || {};

/**
 * Common Utilities
 * @description utilities routines organized by feature
 * @type {object}
 */
Utilities.Common = {

    Routes : {

        redirect(routePath){
            this._processRedirectToRoute(App.Config.setBasePath(routePath));
        },

        redirectWithState(routePath, stateObject){
            this._processRedirectToRoute({
                pathname : App.Config.setBasePath(routePath),
                state: stateObject
            });
        },

        redirectWithQuery(routePath, queryObject){
            this._processRedirectToRoute({
                pathname : App.Config.setBasePath(routePath),
                query: queryObject
            });
        },

        _processRedirectToRoute(routeConfig){
            if(App.Config.cleanUrlsImplemented){
                browserHistory.push(routeConfig);

            }else{
                hashHistory.push(routeConfig);
            }
        }

    }
}