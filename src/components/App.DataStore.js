var App = App || {};

/**
 * Application Data Store
 * @description Proxy for json data loading and access
 * @type {object}
 */
App.DataStore = {

    data : null,

    dataCache : {},

    init(delegateCallbackFn){
        const _self = this;
        $.getJSON( App.Config.dataStoreFile, function( jsonData ) {
            _self.data = jsonData;
            if (typeof delegateCallbackFn === "function") {
                delegateCallbackFn();
            }
        });
    },

    getCategories(){
        return this._mapItemsToCategory()
    },

    getCategoryItems(categoryId){
        const dataStore = this._mapItemsToCategory();
        const filteredSet = _.findWhere(dataStore, {Id: parseInt(categoryId)});
        return (!!filteredSet) ? filteredSet : [];
    },

    getItem(productItemId){
        const dataStore = this.data.Products.slice();
        const filteredItem = _.findWhere(dataStore, {Id: parseInt(productItemId)});
        return (!!filteredItem) ? filteredItem : null;
    },

    _mapItemsToCategory(){
        /* employ data caching to negate reprocessing static data */
        const cacheKey = 'filteredProductCategories';
        const cachedData = this._pullFromDataCache(cacheKey);
        if(cachedData !== null){
            return cachedData;
        }

        const filteredProductCategories = this._processMapItemsToCategory();
        this._addToDataCache(cacheKey, filteredProductCategories);

        return filteredProductCategories;
    },

    _processMapItemsToCategory(){
        const filteredProductCategories = this.data.ProductCategories.slice();
        for(let item of filteredProductCategories) {
            item.ProductList = _.filter(this.data.Products,
                function (product) {
                    return _.contains(item.Products, product.Id);
                });
        }

        return filteredProductCategories;
    },

    _addToDataCache(cacheKey, data){
        if(!this.dataCache.hasOwnProperty(cacheKey) && !!data){
            this.dataCache[cacheKey] = data;
        }
    },

    _pullFromDataCache(cacheKey){
        if(this.dataCache.hasOwnProperty(cacheKey)){
            return this.dataCache[cacheKey];
        }
        return null;
    }
};


