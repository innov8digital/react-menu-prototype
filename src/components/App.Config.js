var App = App || {};

/**
 * Application Configuration
 * @description Path and Clean URL definitions for development environment
 * @type {object}
 */
App.Config = {
    isDevelopmentEnvironment : (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1'),

    cleanUrlsImplemented : false,

    developmentPathname : function(){
        var localDevelopmentRootPath = '/Wendys.Prototype'; /* for clean urls, set property cleanUrlsImplemented = true */
        return this.cleanUrlsImplemented ? localDevelopmentRootPath : ''
    },

    setBasePath : function(definedPath){
        var basePath = definedPath;
        if(App.Config.isDevelopmentEnvironment){
            basePath = this.developmentPathname() + definedPath;
        }

        return basePath;
    },

    dataStoreFile : 'src/data/ProductDataStore.json',

    imagePath : 'src/images/'
};