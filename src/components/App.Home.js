let App = App || {};

App.Home = class extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <span>
                <Layout.Header>
                    <App.Navigation />
                </Layout.Header>
                <main  id="wrapper">
                    {this.props.children}
                    <div className="push"></div>
                </main>
                <Layout.Footer />
            </span>
        );
    }
};

App.Home.propTypes = {
    children: React.PropTypes.node.isRequired,
};
