let App = App || {};
const ReactRouter = window.ReactRouter;
const {browserHistory, hashHistory, IndexLink, Link, IndexRoute, Route, Router, Redirect} = ReactRouter;

/**
 * Application Router
 * @description Configurable router that can use either hash or clean urls
 * @type {jsx}
 */
App.Router = class extends React.Component {
    constructor(){
        super();
    }

    render () {
        /* history context collection type dependent on clean url configuration property :
        * clean urls use browserHistory, and require server configuration to provide history
        * the ability to manually enter a route without receiving a 404 (i.e. bookmarks of
        * deep links will break without url rewrite in server)
        */
        const historyContext = (App.Config.cleanUrlsImplemented) ? browserHistory : hashHistory;

        return (
            <Router history={historyContext}>
                <Route path={App.Config.setBasePath('/')} component={App.Home}>
                    <IndexRoute component={Menu.Home} />
                    <Route path={App.Config.setBasePath('/menu/:categoryId/category')} component={Menu.SelectedCategory} />
                    <Route path={App.Config.setBasePath('/menu/:productId/featured')} component={Menu.Item} />
                    <Route path={App.Config.setBasePath('/locations')} component={Locations.Home} />
                    <Route path='*' component={Layout.NotFound} />
                </Route>
            </Router>
        );
    }
};


/* Throttling to allow data to load for prototype : actual implementation would be chained sequence */
window.setTimeout(()=>{
    App.DataStore.init(()=>{
        ReactDOM.render(
            <App.Router />,
            document.querySelector('#stage')
        );

    });
}, 1000);

