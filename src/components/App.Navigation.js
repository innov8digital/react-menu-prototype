let App = App || {};

/**
 * Application Navigation
 * @description Navigation JSX template
 * @type {jsx}
 */
App.Navigation = class extends React.Component{
    constructor(){
        super();
    }

    render(){
        return (
            <nav>
                <ul>
                    <li><IndexLink to={App.Config.setBasePath('/')} activeClassName="active" onlyActiveOnIndex={true}>Menu</IndexLink></li>
                    <li><Link to={App.Config.setBasePath('/locations')} activeClassName="active">Locations</Link></li>
                </ul>
            </nav>
        );
    }
};